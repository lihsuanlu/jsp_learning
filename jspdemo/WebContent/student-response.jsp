<html>

<head><title>Student Confirmation Title</title></head>

<body>
	The student is confirmed: ${param.firstName} ${param.lastName}  
	
	<br/><br/>
	
	The student's country: ${param.country }
	
	<br/><br/>
	
	The student's favorite programming language: ${param.favoriteLanguage }
	
	<br/><br/>
	
	<!--  display list of "favoriteLanguage" -->
	<ul>
		<%
			String[] langs = request.getParameterValues("favoriteLanguages");
			for(String tempLang : langs) {
				out.println("<li>" + tempLang + "</li>");
			}
		%>
	</ul>
</body>

</html>