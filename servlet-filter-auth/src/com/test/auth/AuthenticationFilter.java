package com.test.auth;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class AuthenticationFilter
 */
@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {
	
	private String username = "john";
	private String password = "test";
	
	private UserDbUtil userDbUtil;

    /**
     * Default constructor. 
     */
    public AuthenticationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String user = request.getParameter("username");
		String pass = request.getParameter("password");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(false);
		String uri = req.getRequestURI();
		try {
			if(uri.contains("Logout")) {
				chain.doFilter(req, res);
			} else if(uri.contains("login")) {
				if(session.getAttribute("username") == null) {
					chain.doFilter(req, res);
				} else {
					RequestDispatcher dispatcher = req.getRequestDispatcher("/homepage.jsp");
					dispatcher.forward(req, res);
				}
			} else {
				if(session.getAttribute("username") == null && (!userDbUtil.checkCredentials(user, pass))) {
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else {
					if(user != null && userDbUtil.checkCredentials(user, pass)) {
						session.setAttribute("username", user);
					}
					chain.doFilter(req, res);
				}
			}
		} catch(Exception exc) {
			System.out.println(exc);
		}
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
