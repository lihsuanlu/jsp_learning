package com.test.auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

public class UserDbUtil {

	private DataSource dataSource;
	
	public UserDbUtil(DataSource theDataSource) {
		dataSource = theDataSource;
	}
	
	public boolean checkCredentials(String userName, String password) throws Exception {
		if(userName == null || password == null) {
			return false;
		}
		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
			myConn = dataSource.getConnection();
			String sql = "SELECT * FROM user WHERE username = ?";
			myStmt = myConn.prepareStatement(sql);
			myStmt.setString(1, userName);
			myRs = myStmt.executeQuery();
			if(myRs.next()) {
				if(myRs.getString("password").equals(password)) {
					return true;
				}
			}
		} finally {
			close(myConn, myStmt, myRs);
		}
		return false;
	}

	private void close(Connection myConn, PreparedStatement myStmt, ResultSet myRs) throws Exception {
		try {
			if(myConn != null) {
				myConn.close();
			}
			if(myStmt != null) {
				myStmt.close();
			}
			if(myRs != null) {
				myRs.close();
			}
		} catch (Exception exc) {
			throw(exc);
		}
		
	}
}
