package com.test.session;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserDbUtil userDbUtil;
    
	@Resource(name="jdbc/session_user")
	private DataSource dataSource;
	
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		
		try {
			userDbUtil = new UserDbUtil(dataSource);
		} catch (Exception exc) {
			throw new ServletException(exc);
		}
	}
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String command = request.getParameter("command");
		if(command == null) {
			command = "LOGIN";
		}
		System.out.println(command);
		switch(command) {
			case "LOGIN":
				login(request,response);
				break;
			case "LOGOUT":
				logout(request,response);
				break;
			default:
				login(request,response);
		}
		
	}

	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		session.invalidate();
//		session.setAttribute("status", null);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/login.jsp");
		dispatcher.forward(request, response);
	}

	private void login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// TODO Auto-generated method stub
//		System.out.println("user name");
//		System.out.println(request.getParameter("userName"));
//		Enumeration<String> attributes = request.getAttributeNames();
//		Enumeration<String> parameters = request.getParameterNames();
//		System.out.println(request);
		boolean isPresent = false;
		try {
			isPresent = userDbUtil.checkCredentials(request.getParameter("userName"), request.getParameter("password"));
		} catch (Exception exc) {
			System.out.println(exc.getMessage());
		}
		if(request.getParameter("command") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		} else {
			if(isPresent) {
				HttpSession session = request.getSession();
				session.setAttribute("status", "correct");
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher("/homepage.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
