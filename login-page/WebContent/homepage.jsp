<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<body>

<%
	if(session.getAttribute("status") == null) {
		response.sendRedirect("/login-page/login.jsp");
	}
%>

<h2>This is the homepage.</h2>

<p><%= session.getAttribute("status") %></p>

<c:url var="logout" value="UserServlet">
	<c:param name="command" value="LOGOUT" />
</c:url>

<a href="${logout}">Log Out</a>

</body>

</html>